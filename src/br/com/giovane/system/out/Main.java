package br.com.giovane.system.out;

public class Main {

    // attributes for printf
    static int idade = 19;
    static String name = "Giovane";
    static double weight = 69.0;
    static boolean gorgeous = true;
    static char letter = 'G';

    // main method
    public static void main(String[] args) {

        // method creates a new line and positions the cursor on the line below, which is identified by the ending ln
        System.out.println("Your text is inserted here, enclosed in double quotes");

        // does not create a new line and cursor remains on the same line
        System.out.print("Your text is inserted here, enclosed in double quotes");

        // the printf method argument is a Format String that can consist of fixed text and format specifiers. f is formatted
        System.out.printf("%d %s %1f %b %c",idade, name, weight, gorgeous, letter);

        /*

        %d	represents int
        %f	represents float
        %2f	represents double
        %b	represents boolean
        %c	represents char

        */


    }

}
